const express = require("express");
const apiHandlerNotFound = require("../middlewares/api-handler-not-found");
let data = require("../db/data.json")

const api = express.Router();
const users = [];

// Skeleton API CRUD USERS.
api.get("/users", (req, res) => {
    res.status(200).json(data)
});

api.post("/users", (req, res) => {
    const { username, address } = req.body;
    const id = data[data.length - 1].id + 1
    const post = {
        id, username, address
    }
    data.push(post)

    res.status(201).json(data)
});

api.patch("/users/:id", (req, res) => {
    let post = data.find(i => i.id === + req.params.id)
    const params = {username: req.body.username, address: req.body.address} 
    post = { ...post, ...params}
    data = data.map(i => i.id === post.id ? post : i)
    res.status(200).json(data);

});

api.delete("/users/:id", (req, res) => {
    data = data.filter(i => i.id !== +req.params.id)
    res.status(200).json({
        mesesage: `Post dengan id ${req.params.id} sudah berhasil dihapus!`
    });
});

api.use(apiHandlerNotFound);
module.exports = api;