const express = require('express');
const webHandlerNotFound = require('../middlewares/web-handler-not-found');
const web = express.Router();

web.get("/", (req, res) => {
    res.render("main");
});

web.get("/game", (req, res) => {
    res.render("game");
});

web.get("/profile", (req, res) => {
    res.render("profile");
});

web.get("/login", (req, res) => {
    res.render("user", { account: "Dwiky" });
});

web.use(webHandlerNotFound);
module.exports = web;

